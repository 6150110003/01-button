package com.example.task3;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    View.OnClickListener myClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.button1) {
                Toast.makeText(MainActivity.this, "You Click ONE",
                        Toast.LENGTH_SHORT).show();
            } else if (view.getId() == R.id.button2) {
                Toast.makeText(MainActivity.this, "You Click TWO",
                        Toast.LENGTH_SHORT).show();
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button bt1 = (Button) findViewById(R.id.button1);
        Button bt2 = (Button) findViewById(R.id.button2);
        bt1.setOnClickListener(myClickListener);
        bt2.setOnClickListener(myClickListener);
    }
}